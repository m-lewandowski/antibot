<?php
/**
 * Created by PhpStorm.
 * User: Mateusz
 * Date: 10.07.2019
 * Time: 20:30
 */

use M6Web\Component\RedisMock\RedisMockFactory;
use PHPUnit\Framework\TestCase;
use Inis\AntiBot\AntiBot;
use Predis\Client;

final class AntiBotTest extends TestCase
{
    /**
     * @var Predis\Client $redis
     */
    protected $redis;

    protected function setUp(): void
    {
        $factory = new RedisMockFactory();
        $myRedisMockClass = $factory->getAdapterClass('Predis\Client');
        $this->redis = new $myRedisMockClass();
    }

    protected function tearDown(): void
    {
        $this->redis->flushdb();
        unset($this->redis);
    }

    public function testShouldReturnIp()
    {
        $ip = "192.168.1.1";
        $CONF['ip_blocker'] = [];

        $_SERVER['REMOTE_ADDR'] = $ip;
        $antiBot = new AntiBot($CONF['ip_blocker'], $_SERVER, $this->redis);

        $this->assertEquals($ip, $antiBot->getIp());
        unset($redisMock);
    }

    public function testShouldReturnIpFromProxy()
    {
        $ip = "127.0.0.1, 192.168.1.1";
        $CONF['ip_blocker'] = [];

        $_SERVER['REMOTE_ADDR'] = "127.0.0.1";
        $_SERVER['HTTP_X_FORWARDED_FOR'] = $ip;
        $antiBot = new AntiBot($CONF['ip_blocker'], $_SERVER, $this->redis);

        $this->assertEquals("192.168.1.1", $antiBot->getIp());
    }

    public function testShouldBlockIpWhenLimitReached()
    {
        $ip = "192.168.1.1";
        $CONF['ip_blocker']['octets_per_subnet'] = 3;

        $CONF['ip_blocker']['ip_limit'] = 2;
        $CONF['ip_blocker']['ip_ttl'] = 2;
        $CONF['ip_blocker']['subnet_limit'] = 100;
        $CONF['ip_blocker']['subnet_ttl'] = 100;
        $i = 1;
        do {
            $_SERVER['REMOTE_ADDR'] = $ip;
            $antiBot = new AntiBot($CONF['ip_blocker'], $_SERVER, $this->redis);
            $isBot = $antiBot->isBot();
            $i++;
            unset($antiBot);
        } while ($i <= $CONF['ip_blocker']['ip_limit'] + 1);

        $this->assertTrue($isBot);
    }

    public function testShouldBlockSubnetWhenLimitReached()
    {
        $subnet = "192.168.1";
        $CONF['ip_blocker']['octets_per_subnet'] = 3;

        $CONF['ip_blocker']['ip_limit'] = 100;
        $CONF['ip_blocker']['ip_ttl'] = 100;
        $CONF['ip_blocker']['subnet_limit'] = 3;
        $CONF['ip_blocker']['subnet_ttl'] = 5;

        $i = 1;
        do {
            $_SERVER['REMOTE_ADDR'] = $subnet . "." . $i;
            $antiBot = new AntiBot($CONF['ip_blocker'], $_SERVER, $this->redis);
            $isBot = $antiBot->isBot();
            $i++;
            unset($antiBot);
        } while ($i <= $CONF['ip_blocker']['subnet_limit'] + 1);

        $this->assertTrue($isBot);
    }

    public function testShouldNotBlockIpWhenIpOnWhitelist()
    {
        $ip = "192.168.1.1";
        $CONF['ip_blocker']['octets_per_subnet'] = 3;

        $CONF['ip_blocker']['ip_limit'] = 2;
        $CONF['ip_blocker']['ip_ttl'] = 2;
        $CONF['ip_blocker']['subnet_limit'] = 100;
        $CONF['ip_blocker']['subnet_ttl'] = 100;
        $CONF['ip_blocker']['whitelist'] = [$ip];

        $i = 1;
        do {
            $_SERVER['REMOTE_ADDR'] = $ip;
            $antiBot = new AntiBot($CONF['ip_blocker'], $_SERVER, $this->redis);
            $isBot = $antiBot->isBot();
            $i++;
            unset($antiBot);
        } while ($i <= $CONF['ip_blocker']['ip_limit'] + 1);

        $this->assertFalse($isBot);
    }

    public function testShouldNotBlockSubnetWhenSubnetOnWhitelist()
    {
        $subnet = "192.168.1";
        $CONF['ip_blocker']['octets_per_subnet'] = 3;

        $CONF['ip_blocker']['ip_limit'] = 100;
        $CONF['ip_blocker']['ip_ttl'] = 100;
        $CONF['ip_blocker']['subnet_limit'] = 3;
        $CONF['ip_blocker']['subnet_ttl'] = 5;
        $CONF['ip_blocker']['whitelist'] = [$subnet];

        $i = 1;
        do {
            $_SERVER['REMOTE_ADDR'] = $subnet . "." . $i;
            $antiBot = new AntiBot($CONF['ip_blocker'], $_SERVER, $this->redis);
            $isBot = $antiBot->isBot();
            $i++;
            unset($antiBot);
        } while ($i <= $CONF['ip_blocker']['subnet_limit'] + 1);

        $this->assertFalse($isBot);
    }

    public function testShouldBlockIpWhenIpOnBlacklist()
    {
        $ip = "192.168.1.1";
        $CONF['ip_blocker']['octets_per_subnet'] = 3;

        $CONF['ip_blocker']['ip_limit'] = 2;
        $CONF['ip_blocker']['ip_ttl'] = 2;
        $CONF['ip_blocker']['subnet_limit'] = 100;
        $CONF['ip_blocker']['subnet_ttl'] = 100;
        $CONF['ip_blocker']['blacklist'] = [$ip];

        $_SERVER['REMOTE_ADDR'] = $ip;
        $antiBot = new AntiBot($CONF['ip_blocker'], $_SERVER, $this->redis);
        $isBot = $antiBot->isBot();

        $this->assertTrue($isBot);
    }

    public function testShouldBlockSubnetWhenSubnetOnBlacklist()
    {
        $subnet = "192.168.1";
        $ip = "192.168.1.1";
        $CONF['ip_blocker']['octets_per_subnet'] = 3;

        $CONF['ip_blocker']['ip_limit'] = 100;
        $CONF['ip_blocker']['ip_ttl'] = 100;
        $CONF['ip_blocker']['subnet_limit'] = 3;
        $CONF['ip_blocker']['subnet_ttl'] = 5;
        $CONF['ip_blocker']['blacklist'] = [$subnet];

        $_SERVER['REMOTE_ADDR'] = $ip;
        $antiBot = new AntiBot($CONF['ip_blocker'], $_SERVER, $this->redis);
        $isBot = $antiBot->isBot();

        $this->assertTrue($isBot);
    }

    public function testShouldBlockIpWhenOnDynamicBlacklist(){
        $ip = "192.168.1.1";
        $CONF['ip_blocker']['octets_per_subnet'] = 3;

        $CONF['ip_blocker']['ip_limit'] = 2;
        $CONF['ip_blocker']['ip_ttl'] = 2;
        $CONF['ip_blocker']['subnet_limit'] = 100;
        $CONF['ip_blocker']['subnet_ttl'] = 100;
        $i = 1;
        do {
            $_SERVER['REMOTE_ADDR'] = $ip;
            $antiBot = new AntiBot($CONF['ip_blocker'], $_SERVER, $this->redis);
            $isBot = $antiBot->isBot();
            $i++;
            unset($antiBot);
        } while ($i <= $CONF['ip_blocker']['ip_limit'] + 2);

        $this->assertTrue($isBot);
    }
}