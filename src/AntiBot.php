<?php

namespace Inis\AntiBot;

use Predis\Client;

class AntiBot
{
    private $ip;
    private $subnet;
    private $storage;
    private $configuration;
    private $server;

    /**
     * AntiBot constructor.
     * @param array $configuration
     * @param array $server
     * @param Client $storage
     */
    public function __construct(array $configuration, array $server, Client $storage)
    {
        $this->configuration = $configuration;
        $this->storage = $storage;
        $this->server = $server;

        $this->readIP();
        if(empty($this->configuration['whitelist']) || !is_array($this->configuration['whitelist'])){
            $this->configuration['whitelist'] = [];
        }
        if(empty($this->configuration['blacklist']) || !is_array($this->configuration['blacklist'])){
            $this->configuration['blacklist'] = [];
        }
        if(empty($this->configuration['octets_per_subnet'])){
            $this->configuration['octets_per_subnet'] = 2;
        }
        if(!isset($this->configuration['check_redis_prefix'])){
            $this->configuration['check_redis_prefix'] = 'cl_';
        }
        if(!isset($this->configuration['block_redis_prefix'])){
            $this->configuration['block_redis_prefix'] = '';
        }
        $this->subnet = implode(".", array_slice(explode(".", $this->ip), 0, $this->configuration['octets_per_subnet']));
    }

    private function readIP(){
        $this->ip = $this->server['REMOTE_ADDR'];

        if (!empty($this->server['HTTP_VIA']) || !empty($this->server['HTTP_X_FORWARDED_FOR'])) {
            $aHeaders = array(
                'HTTP_FORWARDED',
                'HTTP_FORWARDED_FOR',
                'HTTP_X_FORWARDED',
                'HTTP_X_FORWARDED_FOR',
                'HTTP_CLIENT_IP'
            );
            foreach ($aHeaders as $header) {
                if (!empty($this->server[$header])) {
                    $this->ip = $this->server[$header];
                    break;
                }
            }
            $this->ip = preg_replace("/for=/", "", $this->ip);
            $ipArray = preg_split("/,\s*/", $this->ip);
            if(!empty($ipArray) && is_array($ipArray)){
                $this->ip = $ipArray[count($ipArray)-1];
            }
        }
    }

    private function isIpOnStaticBlacklist(){
        return in_array($this->ip, $this->configuration['blacklist']) || in_array($this->subnet, $this->configuration['blacklist']);
    }

    private function isIpOnStaticWhitelist(){
        return in_array($this->ip, $this->configuration['whitelist']) || in_array($this->subnet, $this->configuration['whitelist']);
    }

    private function isIpOnDynamicBlacklist(){
        return $this->storage->exists($this->configuration['block_redis_prefix'].$this->subnet) || $this->storage->exists($this->configuration['block_redis_prefix'].$this->ip);
    }

    private function isClientLanguageOrGeoIPAccepted(){
        return !empty($this->configuration['accept_language_preg']) && !empty($this->server['HTTP_ACCEPT_LANGUAGE']) && preg_match($this->configuration['accept_language_preg'], $this->server['HTTP_ACCEPT_LANGUAGE'])
            || !empty($this->configuration['accept_country_preg']) && !empty($this->server['GEOIP_COUNTRY_CODE']) && preg_match($this->configuration['accept_country_preg'], $this->server['GEOIP_COUNTRY_CODE']);
    }

    public function getIp(){
        return $this->ip;
    }

    public function isBot(){
        $time = date('Y-m-d H:i:s');

        if($this->isIpOnStaticBlacklist()){
            return true;
        } elseif($this->isIpOnDynamicBlacklist()){
            return true;
        } elseif(!$this->isClientLanguageOrGeoIPAccepted()){
            $ipCount = $this->storage->incr($this->configuration['check_redis_prefix'].$this->ip);

            if($ipCount==1){
                $this->storage->expire($this->configuration['check_redis_prefix'].$this->ip, $this->configuration['ip_ttl']);
            }

            $subnetCount = $this->storage->incr($this->configuration['check_redis_prefix'].$this->subnet);

            if($subnetCount==1){
                $this->storage->expire($this->configuration['check_redis_prefix'].$this->subnet, $this->configuration['subnet_ttl']);
            }

            $ipOnWhiteList = $this->isIpOnStaticWhitelist();

            if($ipCount > $this->configuration['ip_limit'] && !$ipOnWhiteList) {
                $this->storage->set($this->configuration['block_redis_prefix'].$this->ip, $time);
                return true;
            }
            elseif($subnetCount > $this->configuration['subnet_limit'] && !$ipOnWhiteList) {
                $this->storage->set($this->configuration['block_redis_prefix'].$this->subnet, $time);
                return true;
            }
        }
        return false;
    }
}